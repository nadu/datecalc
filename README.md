# README #

This is a web app created with functionality to meet these 5 tasks:

  1. Find out the number of days between two date parameters.
  2. Find out the number of weekdays between two date parameters.
  3. Find out the number of complete weeks between two date parameters.
  4. Accept a third parameter to convert the result of (1, 2 or 3) into one of seconds, minutes, hours, years.
  5. Allow the inclusion of a timezone string for comparison of date parameters from different timezones.


### Notes: ###

Days calculation - this is derived from the date interval object and does not include the end date in the calculation of days

Weekday/Weekend Calculation - this is based on the DatePeriod Object - and loops through the DatePeriod to count the number of weekdays/weekend days. A simple switch can be introduced either through the UI (checkbox) or the constants set in init.php to exclude the date by starting the day counter at 1/-1 based on the dateRange (+ve or -ve)


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up - 
* Configuration
* Dependencies - externally hosted bootstrap, jquery, google font, and locally hosted chosen library
* Database configuration - No database used
* Unit Testing Instructions - Includes few Unit Tests

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Narayan Ramchandani
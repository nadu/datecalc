<?php

class DateCalculatorTest extends \PHPUnit_Framework_TestCase {

    private $instance_DateCalculator;

    /**
     * initialise object during setup
     */
    public function setup() {
        $this->instance_DateCalculator = new aligent\DateCalculator();
    }

    /**
     * test instance creation
     */
    public function testInstanceCreation() {
        $instance_DateCalculator = new aligent\DateCalculator();
        $this->assertInstanceOf('aligent\DateCalculator', $this->instance_DateCalculator);
    }

    /**
     * test getTailoredInterval
     * @dataProvider providergetTailoredIntervalArgs
     * @covers aligent\DateCalculator::getTailoredInterval()
     */
    public function testDateCalculations($date_param1, $timezone_start_val, $date_param2, $timezone_end_val, $primary_interval_type, $expectedResult) {

        $result = $this->instance_DateCalculator->getTailoredInterval($date_param1, $timezone_start_val, $date_param2, $timezone_end_val, $primary_interval_type);
        $this->assertEquals($expectedResult, $result);
        //to confirm test output        
        //var_dump($result);
    }

    /**
     * provide data to test getTailoredIntervsl
     * @return type
     */
    public function providergetTailoredIntervalArgs() {
        return array(
            array('2009-09-29', 'Australia/Adelaide', '2009-09-30', 'Asia/Kolkata', "weekday", array("timeUnit" => "weekday", "timeValue" => 2))
            , array('2009-09-29', 'Asia/Kolkata', '2009-09-27', 'Asia/Kolkata', "day", array("timeUnit" => "day", "timeValue" => -2))
            , array('2002-09-29', '/Sydney', '2002-09-30', 'Asia/Kolkata', "day", array("timeUnit" => "day", "timeValue" => 1))
            , array('2002-09-29', 'Australia/Sydney', '2002-09-27', 'Asia/Kolkata', "day", array("timeUnit" => "day", "timeValue" => -1))
            , array('2002-09-29', '', '2002-09-29', '', "day", "input_error")
            , array('2002-09-29', 'Australia', '2002-09-30', 'Asia/Kolkata', "day", array("timeUnit" => "day", "timeValue" => 1))
            , array('2002-09-29', 'Australia', '2002-09-30', 'Asia/Kolkata', "day", array("timeUnit" => "day", "timeValue" => 1))
            , array(1, 2, 3, 4, 5, "input_error")
            , array(2, 3, 4, 3, "dayweek", "input_error")
        );
    }

    /**
     * test time conversions
     * @dataProvider providertestconvertToTimeUnitArgs
     * @covers aligent\DateCalculator::convertToTimeUnit()
     */
    public function testconvertToTimeUnit($time_value, $time_unit_from, $time_unit_to, $expectedResult) {
        $result = $this->instance_DateCalculator->convertToTimeUnit($time_value, $time_unit_from, $time_unit_to);
        $this->assertEquals($expectedResult, $result);
        //var_dump($result);
    }

    /**
     * test data to test time conversions
     * @return type
     */
    public function providertestconvertToTimeUnitArgs() {
        return array(
            array(7, "W", "D", array("timeUnit" => "D", "timeValue" => 'system_error'))
            , array(7, "day", "second", array("timeUnit" => "second", "timeValue" => 7 * 24 * 60 * 60))
            , array(7, NULL, NULL, array("timeUnit" => NULL, "timeValue" => 'system_error'))
        );
    }

    /**
     * unset initialised object
     */
    public function tearDown() {
        unset($this->instance_DateCalculator);
    }

}

<?php

namespace aligent;

require_once 'init.php';
date_default_timezone_set(DEFAULT_TIMEZONE);

/**
 * Class that contains functions to do date parsing characters. 
 * Created by narayan.ramchandani@gmail.com on 21/06/2015
 */
class DateCalculator {

    public $primary_interval_periods;
    private $interval;
    private $start_date;
    private $start_date_text;
    private $start_date_UTC;
    private $start_date_UTC_text;
    private $end_date;
    private $end_date_text;
    private $end_date_UTC;
    private $end_date_UTC_text;

    /**
     * Set up essential class properties
     */
    public function __construct() {
        $this->primary_interval_periods = array(
            "day" => "Number of days"
            , "weekday" => "Number of weekdays"
            , "week" => "Number of weeks"
//            , "year" => "Number of years"
//            ,"weekend_day" => "Number of weekend days"
        );
    }

    /**
     * Parses two dates based on the date and timezones set
     * Currently setup to work out Years, Months, Weeks, Days, Weekdays, Weekend Days, Hours, Minutes and Seconds
     * @param $date_param1, $timezone_start_val, $date_param2, $timezone_end_val, $primary_interval_type = "weekday"
     * @return array with timeUnit and timeValue indexes
     */
    public function getTailoredInterval($date_param1, $timezone_start_val, $date_param2, $timezone_end_val, $primary_interval_type = "weekday") {
        $returnVal = NULL;
        if ($date_param1 && $date_param2 && $timezone_start_val && $timezone_end_val && array_key_exists($primary_interval_type, $this->primary_interval_periods)) {
            if (
                    ((preg_match("~\d{4}[-/](0?[1-9]|1[0-2])[-/](0?[1-9]|[12][0-9]|3[01])[ ]?(\d{1,2}:\d{1,2}:\d{1,2})?$~", $date_param1)) &&
                    (preg_match("~\d{4}[-/](0?[1-9]|1[0-2])[-/](0?[1-9]|[12][0-9]|3[01])[ ]?(\d{1,2}:\d{1,2}:\d{1,2})?$~", $date_param2))) || ($date_param1 === "now") || ($date_param2 === "now")
            ) {

                if (!$this->isValidTimezone($timezone_start_val)) {
                    $timezone_start_val = "UTC";
                }

                if (!$this->isValidTimezone($timezone_end_val)) {
                    $timezone_end_val = "UTC";
                }

                $this->primary_interval_type = $primary_interval_type;

                $this->start_date = new \DateTime($date_param1, new \DateTimeZone($timezone_start_val));
                $this->start_date_text = (string) $this->start_date->format('D d-m-Y H:i:s, e');

                $this->end_date = new \DateTime($date_param2, new \DateTimeZone($timezone_end_val));
                $this->end_date_text = (string) $this->end_date->format('D d-m-Y H:i:s, e');

                $this->start_date_UTC = $this->start_date;
                $this->start_date_UTC->setTimezone(new \DateTimeZone('UTC'));
                $this->start_date_UTC_text = (string) $this->start_date_UTC->format('D d-m-Y H:i:s, e');

                $this->end_date_UTC = $this->end_date;
                $this->end_date_UTC->setTimezone(new \DateTimeZone('UTC'));
                $this->end_date_UTC_text = (string) $this->end_date_UTC->format('D d-m-Y H:i:s, e');

                $this->interval = $this->start_date->diff($this->end_date);

                $returnVal["timeUnit"] = $primary_interval_type;

                $past_multiplier = ($this->interval->invert) ? -1 : 1;

                switch ($primary_interval_type) {
                    case "year":
                    case "Y": {
                            $returnVal["timeValue"] = $this->interval->y;
                            break;
                        }
                    case "month":
                    case "M": {
                            $returnVal["timeValue"] = ($this->interval->y * 12) + ($this->interval->m);
                            break;
                        }
                    case "day": {
                            $returnVal["timeValue"] = max(($this->start_date == $this->end_date) ? 0 : 1, $this->interval->days);
                            break;
                        }

                    case "weekday":
                    case "weekend_day": {

                            if ($past_multiplier == -1) {
                                $this->dateRange = new \DatePeriod($this->end_date, new \DateInterval('P1D'), $this->start_date);
                            } else {
                                $this->dateRange = new \DatePeriod($this->start_date, new \DateInterval('P1D'), $this->end_date);
                            }

                            $week_days = 0;
                            $weekend_days = 0;

                            if ($this->interval->days) {

                                foreach ($this->dateRange as $date) {
                                    if ($date->format("N") < 6) {
                                        $week_days++;
                                    } else {
                                        $weekend_days++;
                                    }
                                }
                            }

                            $returnVal["timeValue"] = ($primary_interval_type === "weekday") ? $week_days : $weekend_days;
                            break;
                        }

                    case "week": {
                            $returnVal["timeValue"] = floor($this->interval->days / 7);
                            break;
                        }

                    case "hour":
                    case "minute":
                    case "second": {
                            if ($past_multiplier == -1) {
                                $this->hourly_daterange = new DatePeriod($this->end_date, new DateInterval('PT1H'), $this->start_date);
                            } else {
                                $this->hourly_daterange = new DatePeriod($this->start_date, new DateInterval('PT1H'), $this->end_date);
                            }
                            $hours = 0;
                            foreach ($this->hourly_daterange as $date) {
                                $hours++;
                            }
                            switch ($primary_interval_type) {
                                case "hour": {
                                        $returnVal["timeValue"] = $hours;
                                        break;
                                    }

                                case "minute": {
                                        $returnVal["timeValue"] = $hours * 60;
                                        break;
                                    }

                                case "second": {
                                        $returnVal["timeValue"] = $hours * 60 * 60;
                                        break;
                                    }
                            }
                            break;
                        }

                    default: {
                            $this->displayError(SYSTEM_ERROR_MESSAGE_01);
                            break;
                            exit();
                        }
                }
                $returnVal['timeValue']*=$past_multiplier;
            } else {
                //  date_param1 || date_param2 do not meet regex rules
                $returnVal = 'format_error';
                $this->displayError(STANDARD_ERROR_MESSAGE);
            }
        }

        //  date_param1 && date_param2 && timezone_start && timezone_end not set or primary_interval_type is out of allowable/enabled range
        else {
            $this->displayError(STANDARD_ERROR_MESSAGE);
            $returnVal = 'input_error';
        }
        return $returnVal;
    }

    /**
     * returns true if the timezone passed is valid
     * @param type $timezone
     * @return type
     */
    private function isValidTimezone($timezone) {
        return in_array($timezone, timezone_identifiers_list());
    }

    /**
     * displays a pretty error with the message - can be extneded to make it more fine-grained
     * @param type $message
     */
    private function displayError($message) {
        $error_message = '<div class="panel panel-danger center-block-red">
    <div class="panel-heading">
    <h3 class="panel-title">Error</h3>
    </div>';
        $error_message.='<div class="panel-body">';
        $error_message.=$message;
        $error_message.="<br />";
        $error_message.="<br />";
        echo $error_message;
    }

    /**
     * 
     * @param type $calculated_time_array
     * @return string
     */
    public function displayResult(array $calculated_time_array) {
        $calculated_time = $calculated_time_array['timeValue'];
        $calculated_time_unit = $calculated_time_array['timeUnit'];

        $primary_info_result = '<div class="panel panel-success center-block-green">
    <div class="panel-heading">
    <h3 class="panel-title">Result</h3>
    </div>';
        $primary_info_result.='<div class="panel-body">';
        $primary_info_result.="<b>Start Date:</b> " . $this->start_date_text;
        $primary_info_result.="&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;<b>in UTC Format: </b>" . $this->start_date_UTC_text;
        $primary_info_result.="<br />";
        $primary_info_result.="<b>End Date:</b> " . $this->end_date_text;
        $primary_info_result.="&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;<b>in UTC Format: </b>" . $this->end_date_UTC_text;
        $primary_info_result.="<br />";
        $primary_info_result.="<b>Actual Difference:</b> " .
                (($this->interval->y) ? $this->interval->y . " year".($this->interval->y==1?"":"s").", " : "") .
                (($this->interval->m) ? $this->interval->m . " month".($this->interval->m==1?"":"s").", " : "") .
                (($this->interval->d) ? $this->interval->d . " day".($this->interval->d==1?"":"s").", " : "") .
                (($this->interval->h) ? $this->interval->h . " hour".($this->interval->h==1?"":"s").", " : "") .
                (($this->interval->i) ? $this->interval->i . " minute".($this->interval->i==1?"":"s")." and " : "") .
                $this->interval->s . " second".($this->interval->s==1?"":"s").".";
        $primary_info_result.="<br />";
        $primary_info_result.="<br />";
        $primary_info_result.= "<b>" . $this->primary_interval_periods[$this->primary_interval_type] . " between two date parameters.</b>
<br />" . number_format($calculated_time, 0) . " " . str_replace("_", " ", $calculated_time_unit) . ($calculated_time == 1 ? "" : "s") . ".";

        return $primary_info_result;
    }

    /**
     * displays additional time conversions based on user selections
     * @param type $calculated_time_array
     * @param type $additional_interval_type
     * @return string
     */
    public function displayAdditionalTimeConversions(array $calculated_time_array, $additional_interval_type) {

        $calculated_time = abs($calculated_time_array['timeValue']);
        $calculated_time_unit = $calculated_time_array['timeUnit'];
        $additional_info_result = "<b>" . $calculated_time . " " . str_replace("_", " ", $calculated_time_unit) . ($calculated_time_unit == 1 ? "" : "s") . " - converted to additional Intervals:</b>";
        $additional_info_result.= "<ul>";

        foreach ($additional_interval_type as $index => $value) {
            //get additional converted time values
            $additional_calculated_time_array = $this->convertToTimeUnit($calculated_time, $calculated_time_unit, $value);

            $additional_calculated_time = $additional_calculated_time_array['timeValue'];
            $additional_calculated_time_unit = $additional_calculated_time_array['timeUnit'];

            $additional_info_result.="<li type='square'>";

            if (is_array($additional_calculated_time_array) && $additional_calculated_time !== "system_error") {
                $additional_info_result.=number_format($additional_calculated_time, 0, NULL, NULL) . " " . str_replace("_", " ", $additional_calculated_time_unit) . ($additional_calculated_time == 1 ? "" : "s") . ".";
            } else {
                $additional_info_result.=$additional_calculated_time_unit . " calculation currently unavailable";
            }
            $additional_info_result.='</li>';
        }

        $additional_info_result.="</ul>";
        return $additional_info_result;
    }

    /**
     * This function takes in the time_value and the time_unit_from parameters as the key input values, which drives the calculation of the output
     * The reason for splitting this functionality is to loosen coupling and let both this and getTailoredInterval function focus on singular responsibilities
     * Returns array with timeUnit and timeValue indexes
     * Currently setup to derive date periods (time_unit_from) from days, but can also do Years, Months, Hours and Seconds - if required
     * @param type $time_value
     * @param type $time_valueType
     * @param type $time_unit
     */
    private function convertToTimeUnit($time_value, $time_unit_from, $time_unit_to) {

        $returnVal["timeUnit"] = $time_unit_to;

        if ($time_unit_from == $time_unit_to) {
            $returnVal["timeValue"] = $time_value;
            return $returnVal;
            exit;
        }

        if ($time_value == 0) {
            $returnVal["timeValue"] = $time_value;
            return $returnVal;
            exit;
        }

        if ($time_value && $time_unit_from && $time_unit_to) {
            switch ($time_unit_from) {

                case "day":
                case "weekday":
                case "weekend_day": {
                        switch ($time_unit_to) {
                            case "second": {
                                    $returnVal["timeValue"] = ($time_value * 86400);
                                    break;
                                }
                            case "minute": {
                                    $returnVal["timeValue"] = ($time_value * 24 * 60);
                                    break;
                                }
                            case "hour": {
                                    $returnVal["timeValue"] = ($time_value * 24);
                                    break;
                                }
                            case "year": {
                                    $returnVal["timeValue"] = floor($time_value / 365);
                                    break;
                                }
                        }
                        break;
                    }
                case "week": {
                        switch ($time_unit_to) {
                            case "second": {
                                    $returnVal["timeValue"] = ($time_value * 7 * 24 * 86400);
                                    break;
                                }
                            case "minute": {
                                    $returnVal["timeValue"] = ($time_value * 7 * 24 * 60);
                                    break;
                                }
                            case "hour": {
                                    $returnVal["timeValue"] = ($time_value * 7 * 24);
                                    break;
                                }
                            case "year": {
                                    $returnVal["timeValue"] = floor($time_value * 7 / 365);
                                    break;
                                }
                        }
                        break;
                    }
                case "Y":
                case "year": {
                        switch ($time_unit_to) {
                            case "second": {
                                    $returnVal["timeValue"] = ($time_value * 365 * 24 * 86400);
                                    break;
                                }
                            case "minute": {
                                    $returnVal["timeValue"] = ($time_value * 365 * 24 * 60);
                                    break;
                                }
                            case "hour": {
                                    $returnVal["timeValue"] = ($time_value * 365 * 24);
                                    break;
                                }
                            case "day": {
                                    $returnVal["timeValue"] = floor($time_value * 365);
                                    break;
                                }
                        }
                        break;
                    }
                default: {
                        $returnVal["timeValue"] = 'system_error';
                        $this->displayError(SYSTEM_ERROR_MESSAGE_01);
                        break;
                    }
            }
        } else {
            $returnVal["timeValue"] = 'system_error';
            $this->displayError(SYSTEM_ERROR_MESSAGE_01);
        }
        return $returnVal;
    }

    /**
     * Returns the options list of available timezones - for use in search form
     * @param type $selected_timezone
     * @return string
     */
    function timezonelist($selected_timezone = NULL) {
        //         $timezones = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        // using json to try and reduce server load
        $JSON = file_get_contents('time_zones.json');
        $data = json_decode($JSON);
        $timezone_options = '';
        foreach ($data->timezones as $index => $value) {
            $timezone_options.="<option value='" . $value . "' " . (($value === $selected_timezone) ? "selected" : "") . ">" . str_replace("/", " - ", $value) . "</option>\n";
            //replaced / with - to make field search friendly.
        }
        return $timezone_options;
    }

}

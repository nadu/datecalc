<?php

namespace aligent;

//required files
require_once './init.php';
require_once './header.php';
require_once './DateCalculator.php';

/* Tasks
 * 1. Find out the number of days between two date parameters.
  2. Find out the number of weekdays between two date parameters.
  3. Find out the number of complete weeks between two date parameters.
  4. Accept a third parameter to convert the result of (1, 2 or 3) into one of seconds, minutes, hours, years.
  5. Allow the inclusion of a timezone string for comparison of date parameters from different timezones.
 */

//initialise variables
$date_param1 = isset($_REQUEST['date1']) ? (string) $_REQUEST['date1'] : NULL;
$date_param2 = isset($_REQUEST['date2']) ? (string) $_REQUEST['date2'] : NULL;

//set user defined timezone
$timezone_start_val = isset($_REQUEST['timezone_start']) && ($_REQUEST['timezone_start']) ? $_REQUEST['timezone_start'] : DEFAULT_TIMEZONE;
$timezone_end_val = isset($_REQUEST['timezone_end']) && ($_REQUEST['timezone_end']) ? $_REQUEST['timezone_end'] : DEFAULT_TIMEZONE;

//primary interval - mandatory
$primary_interval_type = isset($_REQUEST['primary_interval']) && $_REQUEST['primary_interval'] ? $_REQUEST['primary_interval'] : NULL;

//set the default interval type if not selected by the user
$additional_interval_type = (isset($_REQUEST['additional_interval_type']) && $_REQUEST['additional_interval_type']) ? $_REQUEST['additional_interval_type'] : NULL;
//instantiate DateCalulator Object
$date_parser = new DateCalculator();

//Setup form - action to default handler, easier to debug with GET
//Set to POST for better Security
?>

<form method='GET' action='?'>
    <!--Start Date Row-->
    <div class='row' style='margin-bottom:15px;'>
        <div class='form-group col-md-10'>
            <label for='date1' class='col-sm-3 col-md-4 col-lg-2 control-label'>Start date:</label>
            <div class='col-sm-5 col-md-4 col-lg-3'>
                <input type='date' title='Default Format = YYYY-MM-DD. You can also enter YYYY-MM-DD HH:mm:ss. Use now for the current time.' placeholder='YYYY-MM-DD' value='<?php echo $date_param1; ?>' name='date1' id='date1' required/>
            </div>

            <label for='timezone_start' class='col-sm-3 col-md-4 col-lg-2 control-label'>Timezone:</label>
            <div class='col-sm-5 col-md-4 col-lg-3'>
                <select name='timezone_start' id='timezone_start' class='chosen'><?php echo $date_parser->timezonelist($timezone_start_val); ?></select>
            </div>

        </div>
    </div>

    <!--End Date Row-->
    <div class='row' style='margin-bottom:15px;'>
        <div class='form-group col-md-10'>

            <label for='date2' class='col-sm-3 col-md-4 col-lg-2 control-label'>End date:</label>
            <div class='col-sm-5 col-md-4 col-lg-3'>
                <input type='date' title='Default Format = YYYY-MM-DD. You can also enter YYYY-MM-DD HH:mm:ss. Use now for the current time.' placeholder='YYYY-MM-DD' value='<?php echo $date_param2; ?>' name='date2' id='date2' required/>
            </div>

            <label for='timezone_end' class='col-sm-3 col-md-4 col-lg-2 control-label'>Timezone:</label>
            <div class='col-sm-5 col-md-4 col-lg-3'>
                <select name='timezone_end' id='timezone_end' class='chosen'><?php echo $date_parser->timezonelist($timezone_end_val); ?></select>
            </div>

        </div>
    </div>

    <!--Primary Interval Row-->
    <div class='row' style='margin-bottom:15px;'>
        <div class='form-group col-md-10'>

            <label for='primary_interval' class='col-sm-3 col-md-4 col-lg-2 control-label'>Primary Interval:</label>
            <div class='col-sm-5 col-md-4 col-lg-3'>
                <select name='primary_interval' id='primary_interval' class='chosen' style='min-width:300px;'>
                    <?php
                    $primary_interval_type_options = "";

                    foreach ($date_parser->primary_interval_periods as $index => $value) {
                        $primary_interval_type_options.="<option value='" . $index . "' " . (($primary_interval_type == $index) ? "selected" : "") . ">" . $value . "</option>";
                    }
                    echo $primary_interval_type_options;
                    ?>
                </select>
            </div>
        </div>
    </div>

    <!--Additional Interval Row-->
    <div class='row'>
        <div class='form-group col-md-10'>
            <label for='additional_interval' class='col-sm-3 col-md-4 col-lg-2 control-label'>Additional Interval:</label>
            <div class='col-sm-5 col-md-4 col-lg-3'>
                <select name = 'additional_interval_type[]' class = 'chosen' style = 'min-width:340px;' multiple='true' id='additional_interval'>
                    <?php
                    $additional_interval_periods = array(
                        "second" => "Seconds",
                        "minute" => "Minutes",
                        "hour" => "Hours",
                        "year" => "Years"
                    );
                    //Additional Interval
                    $interval_type_options = "";
                    foreach ($additional_interval_periods as $index => $value) {
                        $interval_type_options.="<option value='" . $index . "' " . (($additional_interval_type) ? ((in_array($index, $additional_interval_type)) ? "selected" : "") : "") . ">" . $value . "</option>";
                    }
                    echo $interval_type_options;
                    ?>
                </select>
            </div>
        </div>
    </div>

    <!--Submit/Reset buttons Row-->
    <div style='margin-top:10px;'>
        <div class='form-group col-md-10 col-md-offset-1'>

            <div class='col-sm-5 col-md-2 col-lg-3'>
                <input name='submit' type='Submit' value='Submit' class='btn btn-success'>
            </div>

            <div class='col-sm-5 col-md-2 col-lg-3'>
                <input name = 'reset_action' type = 'Button' value = 'Reset' onClick = "window.location.href = 'index.php'" class = 'btn btn-danger'>
            </div>
        </div>
    </div>

</form>
</div>
</div>
</div>
<br />
<br />

<?php
//display results
if (isset($_REQUEST['submit'])) {

    if ($primary_interval_type) {

        $calculated_time_array = $date_parser->getTailoredInterval($date_param1, $timezone_start_val, $date_param2, $timezone_end_val, $primary_interval_type);

        if (is_array($calculated_time_array) && $calculated_time_array !== "input_error") {
            echo $date_parser->displayResult($calculated_time_array);

            echo '<br />';
            echo '<br />';

            //additional interval - null or array
            $additional_interval_type = isset($_REQUEST['additional_interval_type']) && ($_REQUEST['additional_interval_type']) ? $_REQUEST['additional_interval_type'] : NULL;

            if ($additional_interval_type && is_array($additional_interval_type)) {
                echo $date_parser->displayAdditionalTimeConversions($calculated_time_array, $additional_interval_type);
            }
            echo '</div>';
        }
    }
}
require_once './footer.php';
